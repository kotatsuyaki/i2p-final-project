\documentclass[12pt,openany]{scrbook}
\usepackage{xeCJK}
\setCJKmainfont{Noto Serif CJK TC}
\setCJKsansfont{Noto Sans CJK TC}
\setCJKmonofont{Noto Serif CJK TC}

\usepackage[margin=2cm]{geometry}
\setlength{\parindent}{12pt}

\usepackage{url,csquotes,listings,lmodern,float,wrapfig,graphicx,tikz,amsmath,nicefrac,subcaption,caption,tabularx,makecell,enumitem,multicol}
\usepackage[T1]{fontenc}
\usepackage[nodayofweek,level]{datetime}
\usepackage[boxruled]{algorithm2e}

\renewcommand{\algorithmautorefname}{Algorithm}
\SetFuncSty{textsc}

\lstset{basicstyle=\footnotesize\ttfamily,columns=fullflexible}

\DeclareMathOperator{\uct}{\mathrm{UCT}}
\newcommand{\argmax}[1]{\underset{#1}{\mathrm{argmax}}}

\begin{document}

\title{Final Project Report}
\subtitle{I2P Course, NTHU Department of CS}
\author{ \textbf{ 17\textsuperscript{th} Team } \vspace{5pt} \\ 107021129 黃明瀧 \\ 107062110 顏廷宇 \\ 107062203 張家瑜 \\ }
\maketitle

\setcounter{tocdepth}{3}
\tableofcontents

\chapter{Basic Skills}

\section{Meaning of Compiler Flags}

\begin{displayquote}
    請解釋下列編譯指令，每一個參數代表的意涵 (10\%)
    \begin{lstlisting}[
        language=C++,
        directivestyle={\color{black}}
        emph={int,char,double,float,unsigned},
        emphstyle={\color{blue}}
    ]
    g++-8 -std=c++17 -O2 -Wall -Wextra -fPIC \
        -I./ -shared AITemplate/Porting.cpp -o ./build/a1.so
    \end{lstlisting}
\end{displayquote}

\begin{enumerate}

    \item \texttt{-std=c++17}

        This flag tells the compiler toolchain to compile with C++17 language standard.
        The C++ language has been standarized and published as several revisions throughout the years of development.
        Newer revisions of standard may introduce new language constructs and make additions to the standard library,
        but may also deprecate or even remove old features from the language.

    \item \texttt{-O2}

        This flag specifies the optimization level.
        Most GCC-compatible compiler front-ends (such as clang for LLVM) have the following optimization levels\footnote{\textit{3.11 Options That Control Optimization}, \url{https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html}}.

        \begin{itemize}


            \item \texttt{-O0}, the ``no optimization'' level.
                This is the default level, which does absolutely no compile time optimizations.
                This level is ecspecially suitable for faster debug builds.

            \item \texttt{-O1}, the ``moderate optimization'' level.
                With this flag on, the compiler will try to make some optimizations without degrading compilation time significantly.

            \item \texttt{-O2}, the ``full optimization'' level.
                This flag enables nearly all supported optimizations except for those that trade binary size with speed.

            \item \texttt{-O3} enables all the optimizations in \texttt{-O2} and also turns on some aggresive optimization techniques
                such as loop unrolling and more vectorization.

        \end{itemize}

    \item \texttt{-Wall} and \texttt{-Wextra}

        The \texttt{-Wall} flag enables most compiler warnings for questionable code that are easy to avoid\footnote{\textit{3.8 Options to Request or Suppress Warnings}, \url{https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html}}.
        The \texttt{-Wextra} enables even more types of warnings that are not enabled by the \texttt{-Wall} flag.

    \item \texttt{-fPIC}

        This codegen flag tells the compiler to generate position-independent code if it's supported by the target\footnote{\textit{3.17 Options for Code Generation Conventions},\\ \url{https://gcc.gnu.org/onlinedocs/gcc/Code-Gen-Options.html}}.
        Position-independent code is a segment of machine code that can be placed at any address in the virtual memory space.
        This flag adds some indirection overheads at load time and / or runtime.
        The \texttt{-fPIC} argument is necessary for building shared libraries,
        since the library can't predict the base address of itself when loaded.

    \item \texttt{-I./}

        The \texttt{-I} flag is for specifying search directories for header files.
        \texttt{-I./} means to add the current directory into the list of search directories so that header files in this directory can be \verb|#include <...>|ed.

    \item \texttt{-shared}

        Produce a shared object.

    \item \texttt{AITemplate/Porting.cpp -o ./build/a1.so}

        These arguments specify the input and output filenames.
\end{enumerate}


\section{Meaning of the \texttt{call} Function Template}
\begin{displayquote}
    請解釋 Game.h 裡面 \texttt{call} 函數的功能 (5\%)
\end{displayquote}

One of the \textit{call} member functions (there are two) looks like this.

\begin{lstlisting}[
    language=C++,
    directivestyle={\color{black}}
    emph={int,char,double,float,unsigned},
    emphstyle={\color{blue}}
]
template <typename Func, typename... Args,
std::enable_if_t<std::is_void<
std::invoke_result_t<Func, AIInterface, Args...>
>::value, int> = 0>
void call(Func func, AIInterface* ptr, Args... args) {
    std::future_status status;
    auto val = std::async(std::launch::async, func, ptr, args...);
    status   = val.wait_for(std::chrono::milliseconds(m_runtime_limit));

    if (status != std::future_status::ready) {
        exit(-1);
    }
    val.get();
}
\end{lstlisting}

From a big picture of view,
this template function calls arbitrary function on the given \texttt{AIInterface*},
and wait for at most one second before collecting the result.

\subsection{The Template Arguments}

There are several things in the template argument list.

\begin{enumerate}

    \item \texttt{typename Func} declares \texttt{Func} as an template parameter. All subsequent usage of \texttt{Func} refers to this type.
    \item \texttt{typename... Args} declares a \textit{parameter pack},
        which makes this template variadic,
        meaning that the \texttt{Args... args} part in the function can contain zero or more elements.
    \item The \verb|std::enable_if_t<...>| nameless value parameter abuses SFINAE to limit this template to be instantiated only for \texttt{Func} types that both
        \begin{itemize}
            \item can be invoked with the provided \texttt{args} and
            \item returns \texttt{void}
        \end{itemize}
        . The trailing \texttt{= 0} gives this nameless parameter a default value so that callers doesn't need to specify it explicitly.
\end{enumerate}

\subsection{The Function Body}

First, a variable of type \verb|std::future_status| is declared to hold the (synchronized) result of the \texttt{async} task.
\texttt{std::async} is then used to call the function inside the shared library with specified parameters.
After that, we call \verb|std::future<T>::wait_for| to give the player agent at most 1 second to complete its task.

Note that inside the project template given by the TAs,
\texttt{exit(-1)} is invoked upon any failed attempt by a player agent to complete its task within the time limit.
However, in reality we need to implement the logic to judge the player agent lost.
This causes a potential problem if the player agent never returns,
since the \texttt{std::future}'s destructor blocks the main thread indefinitely\footnote{Answer to \textit{Confusion about threads launched by std::async with \texttt{std::launch::async} parameter}, Stack Overflow, \url{https://stackoverflow.com/a/32517201}}.

\section{Shared Libraries}

\begin{displayquote}
    請解釋什麼是 Shared library，為何需要 Shared library ，在 Windows 系統上有類似的東西嗎？(10\%)
\end{displayquote}

Shared libraries are shared object files (\texttt{*.so}) that can be reused by multiple processes,
as opposed to static libraries that must be linked into the program image beforehand.
It's adopted by most modern operating systems to ease resource usage as well as maintenance problems.

During linking, the linker must include full copies of static libraries into the runnable image.
As a result, memory and secondary storage space are wasted to store multiple sections of the same library code.
Also, whenever we want to update a static library, everything relying on the library needs to be built again.
Shared libraries mitigate these issues by either loading library symbols at either load-time (by the program loader) or run-time (using APIs provided by the operating system).
In this way, the OS may take advantage of virtual memory and point multiple usages of a shared library to the same shared pages, thereby reducing memory consumption dramatically.
In addition, updating a shared library is as simple as replacing the old \texttt{*.so} file with a new one.

Shared libraries also come with some cost, most notably compatibility issues.
Dynamically linked programs rely on having compatible libraries on the target system.
If a library is changed or removed, the program may not work anymore.
Static libraries don't have this issue because they're baked into the program itself.
This situation makes it difficult to compile a "universal" binary for Linux distributions,
for example, since glibc is not forward-compatible.
Running a binary built against newer versions of glibc on an older system does not work.

On Microsoft Windows operating systems,
the standard format for shared libraries  is named as \textit{dynamic link libraries} (DLLs).

\chapter{The Final Project}

\section{Our AI Algorithm}

\subsection{Introduction}

Our player agent is implemented using a parallelized version of the MCTS (Monte-Carlo Tree Search) approach.
The MCTS algorithm, initially introduced by Rémi Coulom for playing $9\times 9$ Go in 2006,
works in a best-first way to traverse the search space as much as possible.
To handle games with high branch factors, instead of examining all possible moves,
\textbf{random samples} are used to guide the search process.

The algorithm contains a main loop, with four major steps involved in each iteration.
In the beginning of a search, the search tree contains only one node with current state of the game.
By repeating the following steps, the tree is gradually built and explored.

\begin{enumerate}
    \item \textbf{Selection}.
        Based on some statistics,
        it makes best decisions all the way down the tree until it doesn't have enough knowledge to proceed.
        The selection strategy will be discussed later.
    \item \textbf{Expansion}.
        Possible new children (all reachable states from the leaf node) of the node are added to the bottom.
    \item \textbf{Simulation}.
        Based on some policy,
        it conducts a full simulation from the newly expanded state(s).
        This step is commonly called ``rollout'' or ``playout''.
        The simplest way to do simulations, as done in this project,
        is to apply state transitions completely randomly.
        Alternatives such as using hand-crafted or generated heuristics may perform even better.
    \item \textbf{Backpropagation}.
        Upon reaching an ending state of the game, whether it's win or lose,
        it writes the result back to all the ancestors.
\end{enumerate}

The main loop can be terminated at any time when it's deemed appropriate,
which makes MCTS suitable for this project,
since the player agent may try to search as much as it can before the hard time constraint.
Upon termination, the selection policy is used again to select the best move among all feasible ones.

\begin{figure}[h]
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \begin{tikzpicture}[every node/.style={draw,circle}]
            \node[label={[label distance=-10pt]above left:{score}},label={[label distance=-10pt]below right:{visits}},draw=none,fill=none,scale=1.5,transform shape] (k) at (-2, 2) {$\nicefrac{u}{v}$};
            \node (a) at (0, 0) {$\nicefrac{2}{3}$};
            \node[blue,label=left:{$\uct = 2.05$}] (b) at (-1, -2) {$\nicefrac{2}{2}$};
            \node[label=right:{$1.48$}] (c) at (1, -2) {$\nicefrac{0}{1}$};
            \draw (a) -- (b);
            \draw (a) -- (c);
        \end{tikzpicture}
        \caption{Left child is chosen due to high win rate}
    \end{subfigure}
    \hfill%
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \begin{tikzpicture}[every node/.style={draw,circle}]
            \node (a) at (0, 0) {$\nicefrac{2}{3}$};
            \node[blue] (b) at (-1, -2) {$\nicefrac{2}{2}$};
            \node (c) at (1, -2) {$\nicefrac{0}{1}$};
            \node (d) at (-2, -4) {$\nicefrac{0}{0}$};
            \node (e) at (0, -4) {$\nicefrac{0}{0}$};

            \draw (a) -- (b);
            \draw (a) -- (c);
            \draw (b) -- (d);
            \draw (b) -- (e);
            \draw[dashed,->] (d) -- (-2, -5);
        \end{tikzpicture}
        \caption{Left child is \textit{expanded}, and a simulation starts}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \begin{tikzpicture}[every node/.style={draw,circle}]
            \node (a) at (0, 0) {$\nicefrac{2}{4}$};
            \node (b) at (-1, -2) {$\nicefrac{2}{3}$};
            \node (c) at (1, -2) {$\nicefrac{0}{1}$};
            \node (d) at (-2, -4) {$\nicefrac{0}{1}$};
            \node (e) at (0, -4) {$\nicefrac{0}{0}$};

            \draw[ultra thick,->] (b) -- (a);
            \draw (a) -- (c);
            \draw[ultra thick,->] (d) -- (b);
            \draw (b) -- (e);
        \end{tikzpicture}
        \caption{The result is propagated back}
    \end{subfigure}\hfill%
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
        \begin{tikzpicture}[every node/.style={draw,circle}]
            \node (a) at (0, 0) {$\nicefrac{2}{4}$};
            \node[label=left:{$1.63$}] (b) at (-1, -2) {$\nicefrac{2}{3}$};
            \node[blue,label=right:{$1.67$}] (c) at (1, -2) {$\nicefrac{0}{1}$};
            \node (d) at (-2, -4) {$\nicefrac{0}{1}$};
            \node (e) at (0, -4) {$\nicefrac{0}{0}$};

            \draw (a) -- (b);
            \draw (a) -- (c);
            \draw (b) -- (d);
            \draw (b) -- (e);
        \end{tikzpicture}
        \caption{Upon next selection, right subtree is favored instead}
    \end{subfigure}
    \caption{Simplified model of the MCTS algorithm}
\end{figure}

\subsection{UCT}

The selection step is done based on the well-known UCT formula (Upper Confidence bound for Trees).

\begin{equation}
    \uct (n) = \underset{Exploitation}{v_i} + \underbrace{C \sqrt{\frac{\ln{n_p}}{n_c}}}_{Exploration} \tag{UCT}
\end{equation}

Intuitively, the two terms in the formula corresponds to exploitation and exploration respectively.

\begin{itemize}
    \item \textbf{Exploitation}. The more promising (judged by previous simulations) a node is, the higher chance it gets to be searched more times.
        In our implementation, we use $v_i = \dfrac{\text{wins} \times 1 + \text{draws} \times 0.25 + \text{loses} \times 0}{\text{visits}}$
        as the exploitation term.
        Draws are taken into consideration, but with a lower weight to prevent the player agent from favoring draws too much.
    \item \textbf{Exploration}. Simply put, this term is to avoid starvation of those potential nodes currently with low $v_i$ values.
        $n_p$ and $n_c$ are the total visits of its parent and itself, respectively.
        If a child node hasn't been visited before, then the exploration term dominates,
        and hence is likely to be visited.

        The value $C$ is a constant, typically assigned to be $\sqrt{2}$.
\end{itemize}

With these two terms, the MCTS balances between \textbf{trying out new routes} and \textbf{favoring subtrees with better scores so far}.
Eventually, given enough computational power and time, it provably converges to the optimal solution.
This comes from the fact that in the very end when the whole search tree is constructed,
the result is equivalant to conducting a full un-pruned minimax search.

\subsection{Parallelization}

\begin{algorithm}[h]
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}
    \SetKwBlock{DoParallel}{do in parallel}{end}

    \Input{Current game state $s$}
    \Output{Transitioned game state $s'$}

    \DontPrintSemicolon
    \SetNoFillComment

    \;

    % Start of algorithm
    $T \gets$ search tree with root node $s$\;
    \DoParallel{
        \While{There's still time}{
            $n \gets \mathrm{root}(T)$\;

            \tcc{Selection}
            \While{$n$ has children}{
                $n \gets \argmax{m \in \mathrm{children}(n)}\uct(m)$\;
            }

            \tcc{Expansion}
            \If{State of $n$ is not game end}{
                Add all possible children to $n$\;
                $n \gets$ random child of $n$\;
            }

            \tcc{Simulation}
            $z \gets$ state of $n$\;
            \While{$z$ is not game end}{
                Randomly transit $z$ to next state\;
            }

            \tcc{Back propagation}
            \While{$n$ is not null}{
                Increase $n.\text{visit}$ by 1\;
                Increase $n.\text{score}$ by 0, 0.25, or 1\;
                $n \gets$ parent of $n$\;
            }
        }
    }
    \Return state of $\argmax{m \in \text{level 1 of } T}{\uct(m)}$\;
    \label{alg:pmcts}
    \caption{Out parallel MCTS algorithm (lock operations omitted)}
\end{algorithm}

As indicated by studies, parallelizing MCTS is not trivial.
Due to the fact that MCTS is \textbf{sequential by its nature},
parallelization always come with costs.

There are three major types of parallelization for MCTS.

\begin{itemize}
    \item \textbf{Leaf parallelization}.
        In this method, only the simulation step is parallelized to simultaneously simulate multiple games.
    \item \textbf{Root parallelization}.
        In root parallelization, every worker thread has its own search tree, independent of each other.
        All the workers usually get similar results, thereby wasting computational power.
        This method is more suitable for distributed computation,
        since there's no need for locks at all.
    \item \textbf{Tree parallelization}.
        All the workers share the same search tree with the help of locks.
        The locks may be either global or local
        \footnote{There's also a lock-free version, see \textit{A Lock-free Algorithm for Parallel MCTS}, S. Ali Mirsoleimani, Jaap van den Herik, Aske Plaat and Jos Vermaseren},
        depending on implementation.
\end{itemize}

In our project, we implemented tree parallelization with local locks on all the nodes.
The \textit{simulation} step is the most time-consuming one\footnote{The required time of simulation actually drops gradually to zero if the game is about to end.}.
Also, the simulation step is completely unrelated to the search tree itself.
Therefore by parallelizing,
we can fill the gaps where the search tree isn't accessed by other worker threads.
The pseudocode is shown in Algorithm 1.

\subsection{Experimental Results}

\begin{table}[h]
    \centering
    \begin{tabular}{llll} \Xhline{2\arrayrulewidth}
        Players & Number of Games & Win Rate & 95\% Confidence Interval \\ \hline
        \phantom{Parallel} MCTS v.s. Random & 1000 & $99.9\%$ & $(99.7, 100.1)$ \\
        Parallel MCTS v.s. Random & 1000 & $100.0\%$ & - \\
        Parallel MCTS v.s. MCTS & 1000 & $56.7\%$ & $(54.9, 58.5)$ \\ \Xhline{2\arrayrulewidth}
    \end{tabular}
    \caption{Experimental match results}
\end{table}

Throughout the development, we conducted combats between different implementations of the player agent.
Initially, because of a flaw in the game state transition logic,
the pure MCTS algorithm did not perform well against the random player.
The corrected version, completed by the end of May, was able to reliably beat the random player,
with statistics shown above.

The parallelized version has advantage over the single-threaded version of MCTS,
but not that dramatic.
This is possibly due to the fact that in the nearly-ended games,
both agents are able to conduct full searches,
making them equally powerful.

\begin{table}[h]
    \centering
    \begin{tabular}{llll} \Xhline{2\arrayrulewidth}
        Processor & Multi-Thread & Single-Thread & Ratio \\ \hline
        Ryzen R5-2600 (6T) & 140779 & 48837 & 2.88 \\
        Xeon E5-2650 (12T) & 91148 & 32965 & 2.76 \\
        BCM2837, Raspberry Pi 3B+ (4T) & 11905 & 5079 & 2.34 \\ \Xhline{2\arrayrulewidth}
    \end{tabular}
    \caption{Average simulations per second for the first move}
\end{table}

From the results, we can see that the speedup isn't proportional to the degree of parallelism.
A suspected cause for this is the contention of locks.
From the data we gathered from the profiler,
the cycles spent on simulations is approximately $\nicefrac{1}{3}$ of the total cycles,
which matches the result of ratio being capped at around $\leq 3$.

\subsection{Other Failed Attempts}

Several attempts to enhance the MCTS algorithm has been made by us,
but didn't turn out to be good enough.

\subsubsection*{Embedded Domain-Specific Knowledge}

Domain-specific knowledge, such as hand-crafted heuristics,
may help the gameplay performance of MCTS\footnote{\textit{Adding domain knowledge to a monte carlo tree search algorithm in the game of Go}, Wilco Tielman}.
We tried to embed heuristics into the UCT formula used during selection,
but it turned out to make it play worse,
since the search space isn't big enough to justify the overhead of heavy rollouts.

\subsubsection*{WU-UCT}

WU-UCT\footnote{\textit{Watch the Unobserved: A Simple Approach to Parallelizing Monte Carlo Tree Search}, Anji Liu, Jianshu Chen, Mingze Yu, Yu Zhai, Xuewen Zhou, Ji Liu},
introduced in 2019, was an improved version of the original UCT formula
to fix the deficiency of parallelized MCTS.
In parallelized MCTS, it's common that all workers flock into the same node and wait serially,
decreasing the performance.
In WU-UCT, the \textit{number of ongoing simulations} is added into the exploration term,
which helps to expand searched area faster while keeping the convergence property when $n$ is large.

\begin{equation}
    \uct (n) = v_i + C \sqrt{\frac{\ln{o_p + n_p}}{o_c + n_c}} \tag{WU-UCT}
\end{equation}

However, after several trails, we found that this algorithm doesn't work well against the ultimate tic-tac-toe game.
In WU-UCT, an additional step, called \textit{incomplete update},
is required in order propagate the ``ongoing simulation'' information all the way to the root node.
This harms the performance further such that the speedup is decreased to $\leq 2$.
The WU-UCT approach seems more suitable in games where the cost of simulations is higher.

\section{Division of Work and Scheduling}

\subsection*{Division of Work}

\begin{itemize}
    \item 黃明瀧 --- Algorithm implementation
    \item 顏廷宇 --- UI design and testing
    \item 張家瑜 --- UI design and testing
\end{itemize}

\subsection*{Schedule}
This is not real schedule, but rather Git history.

\begin{itemize}
    \item \formatdate{30}{5}{2020} The main framework of the single-thread MCTS player agent was completed.
    \item \formatdate{13}{6}{2020} The interactive text UI was completed.
    \item \formatdate{26}{6}{2020} The multi-thread MCTS player agent was completed.
\end{itemize}

\section{Feedbacks and Lessons Learned}

\subsection*{\textmd{黃明瀧}}

I had a lot of fun doing this final project.
Had it not been for the limit of time,
I'm largely tempted to implement more sophisticated variants of the MCTS algorithm.
I'll probably keep this project around and try to generalize the core part to play arbitrary perfect-information game,
and to see if it performs well.

My experience in implementing the MCTS algorithm dates back to my toy project two years ago,
in which I wrote a simple agent for $3\times 3$ tic-tac-toe game in Rust.
It was an overkill, since the $9! = 362880$ possible configurations\footnote{Actually only 255168 after removing unreachable states, see \url{http://www.se16.info/hgb/tictactoe.htm}}
can easily be searched using brute-force strategy.
I'm bad at gaming, but it was satisfying to see machines perform strategic gameplays for a game that I failed to play well.
This project brought even more pleasure than the mini compiler project (which gave me a chance to try out register allocation and SSA-form optimizations).
{\tiny The second mini project was a mess and was not fun at all. Who will ever want to re-invent containers that are available from the core libraries?}

There are some potential improvements that I haven't got time to try out yet.
One of them is the lock-free version of the parallelized MCTS\footnote{\textit{A Lock-free Algorithm for Parallel MCTS}, S. Ali Mirsoleimani, Jaap van den Herik, Aske Plaat and Jos Vermaseren}.
It utilizes the (lock-free on most platforms) atomic primitives to achieve synchronization without using locks.

\begin{minipage}{\textwidth}
    \par\noindent\centering\rule{0.3\textwidth}{0.4pt}
\end{minipage}

During development of this MCTS player agent,
a friend of mine at NCTU was also trying to use similar algorithms to play the famous board game \textit{Reversi}.
(The game has a significantly higher branch factor compared to the ultimate tic-tac-toe game,
so it benefitted a lot from the parallelization.)
We exchanged a lot of ideas, such as outcome of experiments and coding pitfalls.
Our project wouldn't have made such a rapid progress without him.

\subsection*{\textmd{顏廷宇}}

I learned a lot from the final project,
Thanks to our team leader Huang,
who gave me lots of useful suggestions.
I coped with the UI part.
We didn't use the ncurses library but used the POSIX API.
The \texttt{SIGWINCH} signal is catched to change the layout with terminal's window.
In order to not display the cursor, we used functions from termios.h.
Buffer the output in local,
print and set them all together then output to avoid flashing, but sometimes it still flash.
The difficulty we met when we implemented UI part is that the spec of C++ IO streams don't specify when to flush,
so printing the output row by row causes the screen to flash all time.
The solution is to add another layer of buffering on my own.
Multi-sync thread was fretful. The solution was using a Channel class to package the logic the logic.
We also met difficulties when we tested the UI part, since the running time is too long haha.

\section{Other References}

\begin{itemize}[label={}]
    \item \textit{Bandit based Monte-Carlo Planning}, Levente Kocsis and Csaba Szepesvári
    \item \textit{Software Framework for Parallel Monte Carlo Tree Search}, Ting-Fu Liao, NCTU
    \item \textit{MCTS-Minimax Hybrids}, Hendrik Baier and Mark H. M. Winands, Member, IEEE
\end{itemize}

\end{document}
