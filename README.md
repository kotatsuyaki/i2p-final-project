# Ultimate TTT MCTS Implementation (WIP)

This repository contains the code for our final project for the I2P course at NTHU.

[[_TOC_]]

## Screencast (click to watch)

[![asciicast](https://asciinema.org/a/53WmLBVv4AViIBF5Z9dFMvrON.svg)](https://asciinema.org/a/53WmLBVv4AViIBF5Z9dFMvrON)
