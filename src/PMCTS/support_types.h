#ifndef SUPPORT_TYPES_H
#define SUPPORT_TYPES_H

#include "../Misc/rand_util.h"

#include <atomic>
#include <array>
#include <mutex>
#include <optional>
#include <thread>
#include <atomic>

/**
 * @brief Namespace for all MCTS (Monte-Carlo Tree Search) related utilities
 */
namespace pmcts {

/* Namespace-scoped variables */
extern std::atomic_size_t deinitCount;

/* Enums */
enum class Cell : uint8_t { Empty, Self, Opponent };
enum class BoardStatus { Win, Lose, Wip, Draw };
Cell negate(Cell cell);

/**
 * @brief Data structure representing a single 3x3 mini board
 */
class SubBoard {
  public:
    /**
     * @brief Get reference to cell at (x, y)
     */
    inline Cell& get(size_t x, size_t y) { return arr[y * 3 + x]; }

    BoardStatus updateStatus();
    BoardStatus getStatus() const { return status; }

  private:
    std::array<Cell, 9> arr{};
    BoardStatus status = BoardStatus::Wip;
};

/**
 * @brief Data structure representing a 9x9 game board
 */
class Board {
  public:
    /**
     * @brief Get reference to the internal `SubBoard` at (x, y)
     */
    SubBoard& getSubBoard(size_t x, size_t y) { return arr[y * 3 + x]; }

    /**
     * @brief Get reference to the cell (x, y) inside the 9x9 board directly,
     * without getting the `SubBoard` first. This one is preferred if you don't
     * need to do something special.
     */
    inline Cell& get(size_t x, size_t y) {
        return getSubBoard(x / 3, y / 3).get(x % 3, y % 3);
    }

    /**
     * @brief Update the winner status of this board by examining all subboards
     */
    BoardStatus updateStatus();

    /**
     * @brief Update the winner status of this board by examining all subboards.
     * This function only update the subboard influenced by the move, so it
     * should be faster than `BoardStatus::updateStatus()`.
     */
    BoardStatus updateStatusPartial(std::pair<int, int> move);

    /**
     * @brief Get the winner status of this board
     */
    BoardStatus getStatus() const { return status; };

    /**
     * @brief Mark cell at `move` as `side`. This function is used in
     * simulation.
     */
    void performMove(Cell side, std::pair<int, int> move) {
        get(move.first, move.second) = side;
        updateStatusPartial(move);
    }

    BoardStatus status = BoardStatus::Wip;

  private:
    /**
     * @brief Update status for all subboards
     */
    void updateSubboardStatuses();

    /**
     * @brief Get an array of subboard statuses
     */
    std::array<BoardStatus, 9> getSubboardStatusArr() const;

    /**
     * @brief Update status based on the subboard status array
     */
    BoardStatus updateSelfStatus();

    /**
     * @brief Array of underlying 9 3x3 subboards
     */
    std::array<SubBoard, 9> arr{};
};

/**
 * @brief Data structure representing the state of a game at an instant. It
 * contains the board layout, the next round side, last move etc. Additionally,
 * some fields are for UCT evaluation.
 */
class State {
  public:
    State(
        Board board,
        Cell side,
        std::optional<std::pair<int, int>> move = std::nullopt)
        : board(board), side(side), move(move) {}

    /**
     * @brief Get all possible states from this state as a vector
     */
    std::vector<State> getDerivedStates();

    /**
     * @brief Start playing randomly (by modifying the state itself) until
     * gameover. This is used in the MCTS simulation phase.
     */
    void playRandomly();

    friend class Node;
    friend class PlayerAgent;

  private:
    /**
     * @brief Internal implementation for `getDerivedStatesFull` when the target
     * subboard is already full. It searches the whole 9x9 board for empty
     * cells.
     */
    std::vector<State> getDerivedStatesFull();

    /** Board instant representation */
    Board board;

    /** Side for this round */
    Cell side;

    /** Last move occured */
    std::optional<std::pair<int, int>> move = std::nullopt;

    /** Value for UCT */
    size_t visitCount = 0;

    /** Value for UCT */
    int64_t winScore = 0;
};

class Node {
  public:
    Node(State state) : state(state) {}
    Node(State state, Node* parent) : state(state), parent(parent) {}
    ~Node() {
        for (auto&& child : children) {
            delete child;
        }
    }

    /**
     * @brief Get random child from the children vector. Notice that this
     * function assumes that `children` is not empty, and caller must verify
     * this first.
     */
    inline Node* getRandChild() {
        return *util::selectRandomly(children.begin(), children.end());
    }

    /**
     * @brief Return child with maximum visit count
     */
    std::optional<Node*> getMaxChild();

    friend class PlayerAgent;

  private:
    std::mutex mtx;
    State state;
    std::vector<Node*> children;
    std::optional<Node*> parent = std::nullopt;
    double uctVal;
    size_t simCount = 0;
};

struct Tree {
    Node* root;
    inline Tree() : root(nullptr) {}
    inline Tree(Node* root) : root(root) {}

    inline void setRoot(Node* root) {
        this->root = root;
    }

    inline void clearRoot() {
        deinitCount++;

        // Spin up a thread to recursively recycle the tree nodes.
        // The deletion takes long (up to 0.05s), so we offload it to another
        // thread to avoid wasting the 1 second time quota.
        Node* oldRoot = root;
        root = nullptr;
        std::thread(
            [](Node* root) {
                delete root;
                deinitCount--;
            },
            oldRoot)
            .detach();
    }
};

/**
 * @brief Wrapper class for `std::array` along with last meaningful value's
 * index. No any bounds check are done during operations, so be careful when
 * using this class. This is done for performance concerns.
 */
template <typename T, size_t len> class FixedSizeVec {
  private:
    std::array<T, len> arr{};
    size_t last = 0;

  public:
    inline size_t size() const { return last; }

    inline void append(T&& value) { arr[last++] = value; }
    inline void clear() { last = 0; }

    using IterType = decltype(arr.cbegin());

    inline IterType cbegin() { return arr.cbegin(); }
    inline IterType cend() { return arr.cbegin() + last; }
};
} // namespace pmcts

#endif
