#include "./pmcts_ai.h"
#include "./support_types.h"

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <limits>
#include <mutex>
#include <set>
#include <sstream>
#include <string>

#ifdef __unix__
#include <unistd.h>
#endif

namespace pmcts {

std::atomic_size_t deinitCount{0};

double uct(double ttlVisit, double score, double visit) {
    return (score / (visit + std::numeric_limits<double>::epsilon()) / 4) +
           1.414 * std::sqrt(
                       log(ttlVisit) /
                       (visit + std::numeric_limits<double>::epsilon()));
}

[[deprecated]]
double
wuuct(double ttlVisit, double ttlSim, double visit, double score, double sim) {
    return (score / (visit + std::numeric_limits<double>::epsilon()) / 4) +
           1.414 * std::sqrt(
                       (log(ttlVisit + ttlSim)) /
                       (visit + sim + std::numeric_limits<double>::epsilon()));
}

/**
 * @brief Try to get the physical core count. If it fails, then it will fallback
 * to returning logical core count.
 */
size_t getCoreCount() noexcept {
    size_t threadCount =
#ifdef _SC_NPROCESSORS_ONLN
        sysconf(_SC_NPROCESSORS_ONLN);
#else
        std::max(1, std::thread::hardware_concurrency());
#endif

    try {
        // Linux-only pseudofile
        std::ifstream cpuinfo("/proc/cpuinfo");

        std::string line;
        std::set<std::string> cores;
        std::set<std::string> procs;

        while (std::getline(cpuinfo, line)) {
            if (line.substr(0, 7) == "core id") {
                cores.insert(line);
            }
            if (line.substr(0, 9) == "processor") {
                procs.insert(line);
            }
        }
        return std::clamp(
            std::max(procs.size(), cores.size()),
            static_cast<size_t>(1),
            static_cast<size_t>(12));
    } catch (...) {
        return std::clamp(
            threadCount, static_cast<size_t>(1), static_cast<size_t>(12));
    }
    return std::clamp(
        threadCount, static_cast<size_t>(1), static_cast<size_t>(12));
}

PlayerAgent::PlayerAgent() {
    coreCount = getCoreCount();
}

Node* PlayerAgent::selectNode(Node* root) {
    {
        std::lock_guard<std::mutex> lock(root->mtx);
        if (root->children.size() == 0) {
            return root;
        }
    }

    // The parent node
    Node* node = root;

    // Loop until we reach a leaf node
    while (true) {
        std::lock_guard<std::mutex> lock(node->mtx);

        // Extract visit count of the parent node
        const auto parentVisit = node->state.visitCount;

        for (auto child : node->children) {
            child->mtx.lock();
            child->uctVal = uct(
                parentVisit, child->state.winScore, child->state.visitCount);
        }

        // Select best node by UCT as the new parent
        Node* selected = *std::max_element(
            node->children.begin(),
            node->children.end(),
            [&](Node* lhs, Node* rhs) {
                return lhs->uctVal < rhs->uctVal;
            });

        for (auto child : node->children) {
            child->mtx.unlock();
        }

        node = selected;

        if (node->children.size() == 0) {
            break;
        }
    }

    return node;
}

void PlayerAgent::expand(pmcts::Node* node) {
    const auto derivedStates = node->state.getDerivedStates();
    for (const auto& state : derivedStates) {
        node->children.emplace_back(new Node{state, node});
    }
}

BoardStatus PlayerAgent::simulateRandomly(State initialState) {
    auto tmpState = initialState;
    auto status   = tmpState.board.getStatus();

    // Loop until end of game
    while (status == BoardStatus::Wip) {
        tmpState.side = negate(tmpState.side);
        tmpState.playRandomly();
        status = tmpState.board.getStatus();
    }

    return status;
}

void PlayerAgent::propagateIncomplete(Node* node) {
    std::optional<Node*> optNode = node;
    while (optNode.has_value()) {
        Node* tmp = *optNode;
        std::lock_guard<std::mutex> lock(tmp->mtx);
        tmp->simCount += 1;
        optNode = tmp->parent;
    }
}

void PlayerAgent::propagateBack(Node* node, BoardStatus status) {
    std::optional<Node*> tmpNodeOpt = node;

    while (true) {
        if (!tmpNodeOpt.has_value()) {
            break;
        }

        auto tmpNode = tmpNodeOpt.value();
        std::lock_guard<std::mutex> lock(tmpNode->mtx);

        // Always increase the denominator
        tmpNode->state.visitCount++;

        // Increase the numerator if it's winning
        if (status == BoardStatus::Win) {
            tmpNode->state.winScore += 4;
        }

        if (status == BoardStatus::Draw) {
            tmpNode->state.winScore += 1;
        }

        // Go up one level
        tmpNodeOpt = tmpNode->parent;
    }

    std::lock_guard<std::mutex> lock(node->mtx);
}

std::pair<int, int> PlayerAgent::query() {
    // Setup tree
    searchTree.setRoot(new Node{State{localBoard, Cell::Opponent, lastMove}});

    this->loopCount = 0;

    const auto startTime = std::chrono::system_clock::now();

    std::vector<std::thread> workers;

    // Start workers
    for (size_t i = 0; i < coreCount; i++) {
        workers.emplace_back(&PlayerAgent::workerLoop, this, startTime);
    }

    // Collect workers
    for (auto& worker : workers) {
        worker.join();
    }

    logfile << "loopCount: " << loopCount.load() << "\n";

    auto finalNodeOpt = searchTree.root->getMaxChild();
    if (finalNodeOpt.has_value()) {
        auto moveOpt = finalNodeOpt.value()->state.move;
        if (moveOpt.has_value()) {
            const auto num =
                static_cast<double>(finalNodeOpt.value()->state.winScore);
            const auto denom =
                static_cast<double>(finalNodeOpt.value()->state.visitCount);
            const auto quot = num / denom;
            logfile << quot << "\n";
            logfile.flush();
            localBoard.performMove(Cell::Self, moveOpt.value());
            return moveOpt.value();
        } else {
            throw std::runtime_error("Move is nullopt");
        }
    } else {
        throw std::runtime_error("Final node is nullopt");
    }
}

void PlayerAgent::updateOpponentMove(std::pair<int, int> move) {
    this->lastMove = move;
    this->localBoard.performMove(Cell::Opponent, move);
}

void PlayerAgent::openLogFile(std::string path) {
    logfile.open(path);
    logfile << "worker count: " << coreCount << "\n";
}

void PlayerAgent::reset() {
    lastMove = std::nullopt;
    localBoard = Board{};
}

void PlayerAgent::detachedClearSearchTree() {
    searchTree.clearRoot();
}

void PlayerAgent::workerLoop(
    std::chrono::time_point<std::chrono::system_clock> startTime) {
    using namespace std::chrono_literals;

    while (true) {
        // Loop until time's up
        const bool shouldStop =
            std::chrono::system_clock::now() - startTime >= 800ms;
        if (shouldStop) {
            return;
        }

        this->loopCount += 1;

        // Selection
        Node* selectedNode = selectNode(searchTree.root);
        Node* exploredNode = nullptr;
        std::optional<State> initialState;

        {
            std::lock_guard<std::mutex> lock(selectedNode->mtx);

            if (selectedNode->state.board.getStatus() == BoardStatus::Wip) {
                expand(selectedNode);
            }

            if (selectedNode->children.size() > 0) {
                exploredNode = selectedNode->getRandChild();
                std::lock_guard<std::mutex> lock(exploredNode->mtx);
            } else {
                exploredNode = selectedNode;
            }

            initialState = exploredNode->state;
        }

        // Random playout
        const auto playStat = simulateRandomly(*initialState);

        // Back propagation
        propagateBack(exploredNode, playStat);
    }
}

} // namespace pmcts

void AI::init(bool order) {
    std::stringstream ss;
    ss << "pmcts-" << (order ? "o" : "x") << ".log";

    agent.reset();
    agent.openLogFile(ss.str());
}

void AI::callbackReportEnemy(int x, int y) { agent.updateOpponentMove({x, y}); }

std::pair<int, int> AI::queryWhereToPut([[maybe_unused]] TA::UltraBoard ub) {
    const auto answer = agent.query();
    agent.detachedClearSearchTree();
    return answer;
}

/**
 * @brief Dummy struct to ensure that all threads are terminated before
 * `dlclose()`. This is a dirty hack.
 */
struct Dummy {
    ~Dummy() {
        while (pmcts::deinitCount != 0) {
            std::this_thread::yield();
        }
    }
};

Dummy dummy{};
