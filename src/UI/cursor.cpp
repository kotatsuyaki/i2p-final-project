#include "cursor.h"
#include "stdoutbuf.h"

namespace ui {
const wchar_t* esc        = L"\033[";
const wchar_t* red        = L"\033[31m";
const wchar_t* green      = L"\033[32m";
const wchar_t* yel        = L"\033[33m";
const wchar_t* blue       = L"\033[34m";
const wchar_t* reset      = L"\033[0m";
const wchar_t* showCursor = L"\033[?25h";
const wchar_t* hideCursor = L"\033[?25l";
const wchar_t* bold       = L"\033[1m";
const wchar_t* reverse    = L"\033[7m";

namespace cursor {
void move(size_t row, size_t col) {
    ui::stdoutwss << esc << row << ";" << col << "H";
}

void up(size_t dist) { ui::stdoutwss << esc << dist << "A"; }

void down(size_t dist) { ui::stdoutwss << esc << dist << "B"; }

void right(size_t dist) { ui::stdoutwss << esc << dist << "C"; }

void left(size_t dist) { ui::stdoutwss << esc << dist << "D"; }

void clear() { ui::stdoutwss << esc << "2J"; }

void clearLine() { ui::stdoutwss << esc << "K"; }

void drawBox(
    const FrameCharSet& frameChars, size_t outWidth, size_t outHeight) {
    // Top frame
    ui::stdoutwss << frameChars.topLeft;
    for (size_t col = 2; col < outWidth; col++) {
        ui::stdoutwss << frameChars.top;
    }
    ui::stdoutwss << frameChars.topRight;

    // Side frame
    for (size_t row = 2; row < outHeight; row++) {
        cursor::down(1);
        cursor::left(outWidth);
        ui::stdoutwss << frameChars.left;
        cursor::right(outWidth - 2);
        ui::stdoutwss << frameChars.right;
    }

    // Bottom frame
    cursor::down(1);
    cursor::left(outWidth);
    ui::stdoutwss << frameChars.botLeft;
    for (size_t i = 2; i < outWidth; i++) {
        ui::stdoutwss << frameChars.bot;
    }
    ui::stdoutwss << frameChars.botRight;
}

} // namespace cursor

} // namespace ui
