#ifndef CURSOR_H
#define CURSOR_H

#include <iostream>

namespace ui {

extern const wchar_t* esc;
extern const wchar_t* red;
extern const wchar_t* green;
extern const wchar_t* yel;
extern const wchar_t* blue;
extern const wchar_t* reset;
extern const wchar_t* showCursor;
extern const wchar_t* hideCursor;
extern const wchar_t* bold;
extern const wchar_t* reverse;

struct FrameCharSet {
    wchar_t top   = L'─';
    wchar_t bot   = L'─';
    wchar_t left  = L'│';
    wchar_t right = L'│';

    wchar_t topLeft  = L'┌';
    wchar_t topRight = L'┐';
    wchar_t botLeft  = L'└';
    wchar_t botRight = L'┘';
};

namespace cursor {
void move(size_t row, size_t col);

void up(size_t dist);

void down(size_t dist);

void right(size_t dist);

void left(size_t dist);

void clear();

void clearLine();

void drawBox(const FrameCharSet& frameChars, size_t outWidth, size_t outHeight);
} // namespace cursor

} // namespace ui

#endif /* ifndef CURSOR_H */
