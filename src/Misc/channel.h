#ifndef CHANNEL_H
#define CHANNEL_H value

#include <condition_variable>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <thread>

namespace util {
template <typename T> struct Channel {
    std::queue<T> msgBuf;
    std::mutex msgBufMtx;
    std::condition_variable cv;
    bool msgAvail;
};

template <typename T> class Sender {
  public:
    Sender(std::shared_ptr<Channel<T>> channel) : channel(channel) {}

    inline void send(T&& data) const {
        std::unique_lock<std::mutex> lock(channel->msgBufMtx);
        channel->msgBuf.emplace(data);
        channel->cv.notify_one();
    }

  private:
    std::shared_ptr<Channel<T>> channel;
};

template <typename T> class Receiver {
  public:
    Receiver(std::shared_ptr<Channel<T>> channel) : channel(channel) {}

    inline T recv() const {
        std::unique_lock<std::mutex> lock(channel->msgBufMtx);
        if (!channel->msgBuf.empty()) {
            const T msg = channel->msgBuf.front();
            channel->msgBuf.pop();

            return msg;
        }

        channel->cv.wait(lock, [&] { return !channel->msgBuf.empty(); });

        const T msg = channel->msgBuf.front();
        channel->msgBuf.pop();

        return msg;
    }

    inline std::optional<T> tryPeek() const {
        std::unique_lock<std::mutex> lock(channel->msgBufMtx);
        if (!channel->msgBuf.empty()) {
            const T msg = channel->msgBuf.front();
            return msg;
        } else {
            return std::nullopt;
        }
    }

    inline Sender<T> subscribe() const { return Sender<T>(channel); }

  private:
    std::shared_ptr<Channel<T>> channel;
};

template <typename T> inline std::pair<Sender<T>, Receiver<T>> createChannel() {
    auto channel = std::make_shared<Channel<T>>();
    return {Sender<T>{channel}, Receiver<T>{channel}};
}

} // namespace util

#endif /* ifndef CHANNEL_H */
