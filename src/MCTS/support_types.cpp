#include "support_types.h"

#include <algorithm>
#include <array>
#include <cassert>

namespace mcts {
/*
 * Free functions
 */

Cell negate(Cell cell) {
    if (cell == Cell::Empty)
        return Cell::Empty;
    if (cell == Cell::Self)
        return Cell::Opponent;
    return Cell::Self;
}

/*
 * SubBoard member functions
 */

BoardStatus SubBoard::updateStatus() {
    // clang-format off
    static constexpr std::array<std::array<size_t, 3>, 8> winConfList {{
        {0, 1, 2},
        {3, 4, 5},
        {6, 7, 8},
        {0, 3, 6},
        {1, 4, 7},
        {2, 5, 8},
        {0, 4, 8},
        {2, 4, 6}
    }};

    // Do not update "done" boards
    if (status != BoardStatus::Wip)
        return status;

    for (const auto winConf : winConfList) {
        const bool isWin = std::all_of(winConf.begin(), winConf.end(), [&](const auto idx){
            return arr[idx] == Cell::Self;
        });

        if (isWin == true) {
            return status = BoardStatus::Win;
        }

        const bool isLose = std::all_of(winConf.begin(), winConf.end(), [&](const auto idx){
            return arr[idx] == Cell::Opponent;
        });

        if (isLose == true) {
            return status = BoardStatus::Lose;
        }
    }

    const bool isWip = std::any_of(arr.begin(), arr.end(), [](const auto cell) {
        return cell == Cell::Empty;
    });

    if (isWip == true) {
        return status = BoardStatus::Wip;
    }

    return status = BoardStatus::Draw;
    // clang-format on
}

/*
 * Board member functions
 */

BoardStatus Board::updateStatus() {
    updateSubboardStatuses();
    return updateSelfStatus();
}

void Board::updateSubboardStatuses() {
    for (auto& subBoard : arr) {
        subBoard.updateStatus();
    }
}

std::array<BoardStatus, 9> Board::getSubboardStatusArr() const {
    std::array<BoardStatus, 9> statusArr;
    std::transform(
        arr.begin(),
        arr.end(),
        statusArr.begin(),
        [](const SubBoard& subboard) { return subboard.getStatus(); });

    return statusArr;
}

BoardStatus Board::updateSelfStatus() {
    const auto statusArr = getSubboardStatusArr();

    // Array of all winning configurations
    static constexpr std::array<std::array<size_t, 3>, 8> winConfList{
        {{0, 1, 2},
         {3, 4, 5},
         {6, 7, 8},
         {0, 3, 6},
         {1, 4, 7},
         {2, 5, 8},
         {0, 4, 8},
         {2, 4, 6}}};

    // Do not update "done" boards
    if (status != BoardStatus::Wip)
        return status;

    for (const auto winConf : winConfList) {
        const bool isWin =
            std::all_of(winConf.begin(), winConf.end(), [&](const auto idx) {
                return statusArr[idx] == BoardStatus::Win;
            });

        if (isWin == true) {
            return status = BoardStatus::Win;
        }

        const bool isLose =
            std::all_of(winConf.begin(), winConf.end(), [&](const auto idx) {
                return statusArr[idx] == BoardStatus::Lose;
            });

        if (isLose == true) {
            return status = BoardStatus::Lose;
        }
    }

    const bool isWip =
        std::any_of(statusArr.begin(), statusArr.end(), [&](const auto status) {
            return status == BoardStatus::Wip;
        });

    if (isWip == true) {
        return status = BoardStatus::Wip;
    }

    return status = BoardStatus::Draw;
    // clang-format on
}

BoardStatus Board::updateStatusPartial(std::pair<int, int> move) {
    // Return the status if the game has already ended.
    if (this->status != BoardStatus::Wip)
        return this->status;

    const auto subX          = move.first / 3;
    const auto subY          = move.second / 3;
    auto& influencedSubBoard = getSubBoard(subX, subY);

    const auto oldSubboardStatus = influencedSubBoard.getStatus();
    const auto newSubboardStatus = influencedSubBoard.updateStatus();

    if (oldSubboardStatus != newSubboardStatus) {
        return updateSelfStatus();
    }

    return this->status;
}

/*
 * State member functions
 */

std::vector<State> State::getDerivedStatesFull() {
    static std::vector<State> vec{};
    vec.clear();

    for (size_t x = 0; x < 9; x++) {
        for (size_t y = 0; y < 9; y++) {
            if (board.get(x, y) == Cell::Empty) {
                Board cloned = board;
                cloned.performMove(negate(side), {x, y});
                vec.emplace_back(cloned, negate(side), std::make_pair(x, y));
            }
        }
    }

    return vec;
}

std::vector<State> State::getDerivedStates() {
    static std::vector<State> vec{};
    vec.clear();

    const size_t subX = move->first % 3;
    const size_t subY = move->second % 3;

    for (size_t x = 0; x < 3; x++)
        for (size_t y = 0; y < 3; y++) {
            if (board.getSubBoard(subX, subY).get(x, y) == Cell::Empty) {
                Board cloned = board;
                cloned.performMove(negate(side), {subX * 3 + x, subY * 3 + y});
                vec.emplace_back(
                    cloned,
                    negate(side),
                    std::make_pair(subX * 3 + x, subY * 3 + y));
            }
        }

    if (vec.size() == 0) {
        return getDerivedStatesFull();
    }

    return vec;
}

void State::playRandomly() {
    static FixedSizeVec<std::pair<int, int>, 81> possibleMoves{};
    possibleMoves.clear();

    if (move.has_value()) {
        // Has previous move
        const auto [lastX, lastY] = this->move.value();
        const auto bx             = lastX / 3;
        const auto by             = lastY / 3;

        for (auto x = bx * 3; x < bx * 4; x++) {
            for (auto y = by * 3; y < by * 4; y++) {
                auto cell = board.get(x, y);
                if (cell == Cell::Empty) {
                    possibleMoves.append({x, y});
                }
            }
        }

        // If nothing was appended in the previous loop, iterate over the whole
        // board.
        if (possibleMoves.size() == 0) {
            // The target subboard is full
            for (int x = 0; x < 9; x++) {
                for (int y = 0; y < 9; y++) {
                    if (board.get(x, y) == Cell::Empty) {
                        possibleMoves.append({x, y});
                    }
                }
            }
        }
    } else {
        // No previous move
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                if (board.get(x, y) == Cell::Empty) {
                    possibleMoves.append({x, y});
                }
            }
        }
    }

    if (possibleMoves.size() == 0) {
        // TODO: This may be a bug
        board.status = BoardStatus::Draw;
    } else {
        const auto randomMove =
            *util::selectRandomly(possibleMoves.cbegin(), possibleMoves.cend());
        board.performMove(side, randomMove);
    }
}

/*
 * Node member functions
 */

std::optional<Node*> Node::getMaxChild() {
    if (children.size() == 0) {
        // logfile << "No children\n";
        return std::nullopt;
    }

    // Copy the candidate moves
    auto candidates = children;

    // Sort the candidates (our moves) in descending order
    std::sort(candidates.begin(), candidates.end(), [](auto&& lhs, auto&& rhs) {
        return lhs->state.visitCount > rhs->state.visitCount;
    });

    // Find the first candidate that does not lead to sudden death
    auto bestChild =
        std::find_if(candidates.begin(), candidates.end(), [&](Node*& child) {
            // Opponent moves (side are opponent)
            auto opponentStates = child->state.getDerivedStates();

            // Whether any of the opponent moves lead to opponent's win
            const bool hasSuddenDeath = std::any_of(
                opponentStates.begin(),
                opponentStates.end(),
                [&](State& state) {
                    return state.board.updateStatus() == BoardStatus::Lose;
                });
            return !hasSuddenDeath;
        });

    // If all moves lead to death, we have no choice but return the first child
    if (bestChild != candidates.end()) {
        if (bestChild != candidates.begin()) {
            // logfile << "Avoided sudden death\n";
        }
        return *bestChild;
    } else {
        // logfile << "All moves lead to death\n";
        // logfile.flush();
        return candidates[0];
    }
}
} // namespace mcts
